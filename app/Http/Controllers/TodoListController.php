<?php

namespace App\Http\Controllers;

use App\Model\TodoListModel;
use Illuminate\Http\Request;

class TodoListController extends Controller
{
    /**
     * function to load main page
     *
     * @return mixed
     */
    public function index()
    {
        $list = TodoListModel::loadData();
        $total = TodoListModel::loadTotalData();
        $data = [
            'list' => $list,
            'total' => $total
        ];
        return view('content', $data);
    }

    /**
     * function to load data active
     *
     * @return mixed
     */
    public function todoActive()
    {
        $wheres[] = "active = 'Y'";
        $list = TodoListModel::loadData($wheres);
        $total = TodoListModel::loadTotalData($wheres);
        $data = [
            'list' => $list,
            'total' => $total
        ];
        return view('content', $data);
    }

    /**
     * function to load data complate
     *
     * @return mixed
     */
    public function todoComplete()
    {
        $wheres[] = "complete = 'Y'";
        $list = TodoListModel::loadData($wheres);
        $total = TodoListModel::loadTotalData($wheres);
        $data = [
            'list' => $list,
            'total' => $total
        ];
        return view('content', $data);
    }

    /**
     * function to insert data
     *
     * @return mixed
     */
    public function insertTodo()
    {
        $colVal = [
            'text' => \request()->new_todo,
            'active' => 'Y',
            'complete' => 'N'
        ];
        $insert = new TodoListModel();
        $insert->doInsert($colVal);
        return redirect('/');
    }

    /**
     * function to update data
     *
     * @return mixed
     */
    public function updateTodo()
    {
        $id = \request()->id;
        $todo = TodoListModel::getByReference($id);
        if ($todo['active'] === 'N'){
            $colVal = [
                'active' => 'Y',
                'complete' => 'N'
            ];
        }else{
            $colVal = [
                'active' => 'N',
                'complete' => 'Y'
            ];
        }

        $update = new TodoListModel();
        $update->doUpdate($id, $colVal);
        return redirect('/');
    }

    /**
     * function to clear data all
     *
     * @return mixed
     */
    public function clearTodo()
    {
        $colVal = [
            'active' => 'Y',
            'complete' => 'N'
        ];
        $clear = new TodoListModel();
        $clear->doClear('Y',$colVal);
        return redirect('/');
    }

    /**
     * function to active data all
     * @return mixed
     */
    public function allTodo()
    {
        $colVal = [
            'active' => 'N',
            'complete' => 'Y'
        ];
        $clear = new TodoListModel();
        $clear->doClear('N',$colVal);
        return redirect('/');
    }
}
