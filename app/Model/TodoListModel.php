<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TodoListModel extends Model
{
    /**
     * the attribute for table name
     *
     * @var string
     */
    private $Table = 'todo_list';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'active', 'complete',
    ];

    /**
     * Function to get all record.
     *
     * @param array $wheres To store the list condition query.
     *
     * @return array
     */
    public static function loadData(array $wheres = []): array
    {
        $strWhere = '';
        if (empty($wheres) === false) {
            $strWhere = ' WHERE ' . implode(' AND ', $wheres);
        }
        $query = 'SELECT id, text, active, complete
                  FROM todo_list ' . $strWhere;

        $sqlResults = DB::select($query);

        return self::arrayObjectToArray($sqlResults);
    }

    /**
     * Function to get data by reference value
     *
     * @param integer $referenceValue To store the reference value of the table.
     *
     * @return array
     */
    public static function getByReference(int $referenceValue): array
    {
        $wheres = [];
        $wheres[] = "id =" . $referenceValue;
        $data = self::loadData($wheres);
        if (count($data) === 1) {
            return $data[0];
        }

        return [];
    }

    /**
     * Function to get total record.
     *
     * @param array $wheres To store the list condition query.
     *
     * @return int
     */
    public static function loadTotalData(array $wheres = []): int
    {
        $result = 0;
        $strWhere = '';
        if (empty($wheres) === false) {
            $strWhere = ' WHERE ' . implode(' AND ', $wheres);
        }
        $query = 'SELECT count(DISTINCT (id)) AS total_rows
                        FROM todo_list' . $strWhere;

        $sqlResults = DB::select($query);
        if (count($sqlResults) === 1) {
            $result = (int)self::objectToArray($sqlResults[0])['total_rows'];
        }

        return $result;
    }

    /**
     * function to do insert.
     *
     * @param array $fieldData To store the field value per column.
     *
     * @return void
     */
    public function doInsert(array $fieldData): void
    {
        $colValue = array_merge($fieldData, [
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table($this->Table)->insert($colValue);
    }

    /**
     * function to do update.
     *
     * @param string $primaryKeyValue To store the primary key value.
     * @param array $fieldData To store the field value per column.
     *
     * @return void
     */
    public function doUpdate(string $primaryKeyValue, array $fieldData): void
    {

        $colValue = array_merge($fieldData, [
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table($this->Table)
            ->where('id', $primaryKeyValue)
            ->update($colValue);
    }

    /**
     * function to do clear.
     *
     * @param string $primaryKeyValue To store the primary key value.
     * @param array $fieldData To store the field value per column.
     *
     * @return void
     */
    public function doClear(string $primaryKeyValue, array $fieldData): void
    {

        $colValue = array_merge($fieldData, [
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table($this->Table)
            ->where('complete', $primaryKeyValue)
            ->update($colValue);
    }

    /**
     * Function to parse the array object data to normal array.
     *
     * @param array $arrayObject To store the data that will be parse.
     * @param array $attributes To store the attribute name that will be taken.
     *
     * @return array
     */
    private static function arrayObjectToArray(array $arrayObject, array $attributes = []): array
    {
        $result = [];
        if (empty($arrayObject) === false) {
            foreach ($arrayObject as $obj) {
                $result[] = self::objectToArray($obj, $attributes);
            }
        }

        return $result;
    }

    /**
     * Function to parse the object data to normal array.
     *
     * @param \stdClass $object To store the data that will be parse.
     * @param array $attributes To store the attribute name that will be taken.
     *
     * @return array
     */
    private static function objectToArray($object, array $attributes = []): array
    {
        $result = [];
        if (\is_object($object) === true && $object !== null) {
            if (empty($attributes) === false) {
                foreach ($attributes as $attribute) {
                    $value = '';
                    if (property_exists($object, $attribute) === true) {
                        $value = $object->$attribute;
                    }
                    $result[$attribute] = $value;
                }
            } else {
                $result = get_object_vars($object);
            }
        }

        return $result;
    }


}
