<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'TodoListController@index');
Route::get('/active', 'TodoListController@todoActive');
Route::get('/completed', 'TodoListController@todoComplete');
Route::post('/insert', 'TodoListController@insertTodo');
Route::post('/update', 'TodoListController@updateTodo');
Route::post('/clear', 'TodoListController@clearTodo');
Route::post('/all', 'TodoListController@allTodo');
