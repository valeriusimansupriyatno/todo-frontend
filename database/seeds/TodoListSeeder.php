<?php

use Illuminate\Database\Seeder;

class TodoListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert(['id' => '1', 'text' => 'Projek PHP', 'active' => 'Y', 'complete' => 'N', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => '']);
        DB::table('menu')->insert(['id' => '2', 'text' => 'Projek Java', 'active' => 'N', 'complete' => 'Y', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => '']);
        DB::table('menu')->insert(['id' => '3', 'text' => 'Projek MySql', 'active' => 'Y', 'complete' => 'N', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => '']);
        DB::table('menu')->insert(['id' => '4', 'text' => 'Projek Vue', 'active' => 'Y', 'complete' => 'N', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => '']);
        DB::table('menu')->insert(['id' => '5', 'text' => 'Projek Kotlin', 'active' => 'Y', 'complete' => 'N', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => '']);
    }
}
