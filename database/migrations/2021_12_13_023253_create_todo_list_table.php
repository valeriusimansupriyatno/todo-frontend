<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_list', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('text',256);
            $table->char('active',1)->default('Y');
            $table->char('complete',1)->default('N');
            $table->timestamps();
        });
        \Illuminate\Support\Facades\Artisan::call('db:seed', [
            '--class' => TodoListSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_list');
    }
}
