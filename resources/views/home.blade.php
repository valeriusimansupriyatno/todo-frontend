<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Template • Todo</title>
    <link rel="stylesheet" href="{{asset('assets/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/index.css')}}">
    <!-- CSS overrides - remove if you don't need it -->
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"
            integrity="sha256-DI6NdAhhFRnO2k51mumYeDShet3I8AKCQf/tf7ARNhI=" crossorigin="anonymous"></script>
</head>
<body>
<section class="todoapp">
    <form id="form-new">
        {{--        @csrf--}}
        <header class="header">
            <h1>Super2Do</h1>
            <input class="new-todo" id="new-todo" name="new_todo" placeholder="What needs to be done?" autofocus>
        </header>
        <!-- This section should be hidden by default and shown when there are todos -->
        <section class="main">
            <input id="toggle-all" class="toggle-all" type="checkbox">
            <label for="toggle-all">Mark all as complete</label>
            <ul class="todo-list">
                <!-- These are here just to show the structure of the list items -->
                <!-- List items should get the class `editing` when editing and `completed` when marked as completed -->
                @yield('content')
            </ul>
        </section>
        <!-- This footer should be hidden by default and shown when there are todos -->
        <footer class="footer">
            <!-- This should be `0 items left` by default -->
        @yield('footer')
        <!-- Remove this if you don't implement routing -->
            <ul class="filters">
                <li>
                    <a class="selected" href="{{url('/')}}">All</a>
                </li>
                <li>
                    <a href="{{url('/active')}}">Active</a>
                </li>
                <li>
                    <a href="{{url('/completed')}}">Completed</a>
                </li>
            </ul>
            <!-- Hidden if no completed items are left ↓ -->
            <button id="clear" class="clear-completed">Clear completed</button>
            <button id="all" class="clear-completed">Active All</button>
        </footer>
    </form>
</section>
<!-- Scripts here. Don't remove ↓ -->
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //update todo
        $(".check[data-category]").click(function (event) {
            let elem = $(event.target);
            var id = elem.attr('id');

            $.ajax({
                type: 'POST',
                url: "{{url("/update")}}",
                data: {id: id},
                success: function (data) {
                    console.log(data);
                    location.reload();
                }
            });

        });

        //insert todo
        $('#new-todo').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 13) {
                let form = $("#form-new")[0];
                let data = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{url('/insert')}}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        });

        //clear complete
        $("#clear").click(function (e) {
            var complete = [];
            $("input[name='check']:checked").each(function () {
                complete.push(this.value);
            });

            $.ajax({
                type: 'POST',
                url: "{{url("/clear")}}",
                data: {complete: complete},
                success: function (data) {
                    console.log(data);
                    location.reload();
                }
            });
        });

        //all active
        $("#all").click(function () {
            var complete = [];
            $("input[name='check']").each(function () {
                complete.push(this.value);
            });

            $.ajax({
                type: 'POST',
                url: "{{url("/all")}}",
                data: {complete: complete},
                success: function (data) {
                    console.log(data);
                    location.reload();
                }
            });
        });
    });
</script>
<script src="{{asset('assets/js/app.js')}}"></script>

</body>
</html>
