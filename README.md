<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Tentang Projek Todo Front-End

Menggunakan :

-   PHP 7.2
-   database MySql

## Cara Menggunakan Aplikasi

-   ketikan Text pada form input
-   tekan enter untuk submit text
-   ceklis text untuk commit todo
-   uncheck untuk mengaktifkan todo

menu :

-   menu all commit todo
-   menu all active todo

## Instalasi

Jalankan :

-   php artisan migrate
-   npm install
-   composer install

### Script tambahan

library JavaScript yang membantu mengatur interaksi antara JavaScript dan HTML.

```shell script
<script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
```

```shell script
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"
            integrity="sha256-DI6NdAhhFRnO2k51mumYeDShet3I8AKCQf/tf7ARNhI=" crossorigin="anonymous"></script>
```

## class yang di buat

Controller : TodoListController
Model : TodoListModel
View : - home - content

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
